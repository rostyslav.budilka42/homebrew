django==4.0.6
djangorestframework==3.13.1
python-dotenv==0.20.0
psycopg2-binary==2.9.3
